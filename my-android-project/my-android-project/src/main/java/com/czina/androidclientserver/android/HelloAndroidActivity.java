package com.czina.androidclientserver.android;

import org.springframework.web.client.RestTemplate;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.czina.androidclientserver.Response;

public class HelloAndroidActivity extends Activity {

	private static String TAG = "my-android-project";

	/**
	 * Called when the activity is first created.
	 * 
	 * @param savedInstanceState
	 *            If the activity is being re-initialized after previously being
	 *            shut down then this Bundle contains the data it most recently
	 *            supplied in onSaveInstanceState(Bundle). <b>Note: Otherwise it
	 *            is null.</b>
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.i(TAG, "onCreate");
		setContentView(R.layout.main);
		Button b = (Button) findViewById(R.id.button1);
		b.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
			callServer();
			}
		});
	}

	private void callServer() {
		AsyncTask<String, Void, Void> async = new AsyncTask<String, Void, Void>() {
			protected Void doInBackground(String ... args) {
				RestTemplate r = new RestTemplate(true);
				Response response = r.getForObject(
						"http://192.168.1.142:8080/server-project/jsonendpoint",
						Response.class);
				TextView tv = (TextView) findViewById(R.id.output);
				tv.setText(response.getMessage());
			 return null;
			}
		};
		 async.execute("");
	}
}

package com.czina.serverproject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.czina.androidclientserver.Request;
import com.czina.androidclientserver.Response;

@Controller
@RequestMapping("/jsonendpoint")
public class JsonEndpoint {


	@RequestMapping(method = RequestMethod.PUT)
	public @ResponseBody Response putHello(@RequestBody Request request) {
		Response r = new Response();
		r.setMessage("Hello" + request.getMessage());
		return r;
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public @ResponseBody Response sayHello() {
		Response r = new Response();
		r.setMessage("Hello FERI");
		return r;
	}
	
	
}
